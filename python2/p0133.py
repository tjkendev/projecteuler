# encoding: utf-8
import math

# エラトステネスのふるい
prime = [1] * (10**5+1)
prime[0] = prime[1] = 0
for i in xrange(2, int(math.sqrt(10**5))+1):
    if prime[i]:
        for j in xrange(i*i, 10**5+1, i):
            prime[j] = 0

# 愚直解 --> くっそ時間かかるのでNG
def solve1():
    def R(k):
        return (10**k - 1) / 9

    LIMIT = 10**15
    factor = {}
    first = {}
    for n in xrange(LIMIT):
        r = R(10**n)
        print n

        i = 3
        while i*i <= r and i <= 10**5:
            if r % i == 0:
                if i not in first:
                    first[i] = n
                factor[i] = factor.get(i, 0) + 1
                print "factor", i, factor[i], first[i], factor[i] + first[i]
                while r % i == 0:
                    r /= i
            i += 2
    print sum(i for i in xrange(1, 10**5+1) if i not in factor)

# 高速解
# R(10**k)でfactorはR(10**(k+1))でもfactorとなる性質を利用
# primeのみなので繰り返し二乗法を使うことができる
def solve2():
    k = 10**15
    res = 0
    # 3も引っかかるので除いている
    for i in xrange(4, 10**5+1):
        if prime[i]:
            r = (((pow(pow(10, 10, i), k, i) - 1 + i) % i) * pow(9, i-2, i)) % i
            if r:
                res += i
            else:
                print i
    print res+2+3

solve2()
