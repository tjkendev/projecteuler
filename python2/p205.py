import itertools
def calc(n, k):
    res = {}
    for p in itertools.product(range(1, k+1), repeat=n):
        s = sum(p)
        res[s] = res.get(s, 0) + 1
    return res

p = calc(9, 4)
q = calc(6, 6)
print p, q
total = 0
cnt = 0
for i in xrange(1, 37):
    for j in xrange(1, 37):
        s = p.get(i, 0); t = q.get(j, 0)
        if i > j:
            cnt += s * t
        total += s * t
print cnt / float(total)


