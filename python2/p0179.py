import math

M = 10**7
M2 = int(math.sqrt(M))
memo = [-1]*(M+1)
prime = [True]*(M+1)

memo[0] = 0
memo[1] = 1
prime[0] = prime[1] = False
for i in xrange(2, M+1):
    if prime[i]:
        for j in xrange(i*i, M+1, i):
            prime[j] = False
        memo[i] = 2

def divisor_count(n):
    if memo[n] != -1:
        return memo[n]
    L = int(math.sqrt(n))
    ret = 1
    for x in xrange(2, L+1):
        if n%x == 0:
            n0 = n
            cnt = 0
            while n0%x==0:
                n0 /= x
                cnt += 1
            memo[n] = (cnt+1)*divisor_count(n0)
            return memo[n]
    memo[n] = 1
    return 1
res = [0]*2
for n in xrange(2, M+1):
    res.append(divisor_count(n))
    print n, res[n]
ans = 0
for n in xrange(2, M):
    if res[n]==res[n+1]:
        ans += 1
print ans
