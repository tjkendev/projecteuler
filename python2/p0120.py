

def fast_pow(x, n, mod):
    ret = 1
    while n>0:
        if n&1:
            ret = (ret * x) % mod
        x = (x * x) % mod
        n >>= 1
    return ret
ans = 0
for a in xrange(3, 1001):
    rmax = 0
    print a,
    a2 = a**2
    dic = {}
    for n in xrange(1, a**2+1):
        a11 = fast_pow(a-1, n, a2)
        a12 = fast_pow(a+1, n, a2)
        if (a11, a12) in dic:
            break
        dic[(a11, a12)] = 1
        r = (a11 + a12) % a2
        rmax = max(r, rmax)
    ans += rmax
    print rmax

print ans
