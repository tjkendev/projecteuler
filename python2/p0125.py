import math
M = 10**8

M2 = int(math.sqrt(M))

def sum2pow(n):
    return n*(n+1)*(2*n+1)/6
def is_palindrome(x):
    s = str(x)
    return s == s[::-1]

idx = 0
ans = 0
dic = {}
for x in xrange(1, M2+1):
    xs = sum2pow(x-1)
    for y in xrange(x+1, M2+1):
        s = sum2pow(y) - xs
        if s > M:
            break
        if is_palindrome(s) and s not in dic:
            dic[s] = 1
            idx += 1
            ans += s
            print idx, x, y, s, ans

