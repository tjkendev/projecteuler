count = 0
n = 3
for i in xrange(1, 2**2**n):
    def tau(a, b, c):
        bits = int("%d%d%d" % (a, b, c), 2)
        return (i >> bits) & 1
    ok = 1
    for j in xrange(2**n):
        a = (j >> 2) & 1; b = (j >> 1) & 1; c = j & 1
        if (tau(a, b, c) and tau(b, c, a^(b&c))):
            ok = 0
    if ok:
        print format(i, '0%db' % (2**n))
        count += 1
print count

n = 6
H = lambda X: ((X << 1) & (1 ^ (2**n-1))) | (((X >> (n-1)) ^ ((X >> (n-2)) & (X >> (n-3)))) & 1)
# Lucas Number
memo = {0: 2, 1: 1}
def lucas(n):
    if n in memo:
        return memo[n]
    memo[n] = res = lucas(n-2) + lucas(n-1)
    return res
used = [0]*(2**n)
ans = 1
for i in xrange(2**n):
    if used[i] == 0:
        cur = H(i)
        cnt = 1
        while cur != i:
            cnt += 1
            used[cur] = 1
            cur = H(cur)
        ans *= lucas(cnt)
        print cnt, lucas(cnt)
print ans
